<?php

namespace App\Http\Controllers;

use Datatables;
use App\Models\Pathologist;
use Illuminate\Http\Request;

class PathologistsController extends Controller
{
    public function index()
    {
    	$pathologists = Pathologist::all();
        return view('pathologists.index', compact('pathologists'));
    }

    public function anyData()
    {
        $pathologists = Pathologist::whereHas('roles', function($q){
            $q->where('name', 'pathologist');
        })->select(['id', 'name', 'email', 'work_number']);
        return Datatables::of($pathologists)
            ->addColumn('namelink', function ($pathologists) {
                return '<a href="users/' . $pathologists->id . '" ">' . $pathologists->name . '</a>';
            })
            ->addColumn('edit', function ($pathologists) {
                return '<a href="' . route("users.edit", $pathologists->id) . '" class="btn btn-success"> Edit</a>';
            })
            ->add_column('delete', function ($pathologists) { 
                return '<button type="button" class="btn btn-danger delete_client" data-client_id="' . $pathologists->id . '" onClick="openModal(' . $pathologists->id. ')" id="myBtn">Delete</button>';
            })->make(true);
    }
}
