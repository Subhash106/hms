<?php

namespace App\Http\Controllers;

use Datatables;
use App\Models\Patient;
use Illuminate\Http\Request;

class PatientsController extends Controller
{
    public function index()
    {
    	$patients = Patient::all();
        return view('patients.index', compact('patients'));
    }

    public function anyData()
    {
        $patients = Patient::whereHas('roles', function($q){
		    $q->where('name', 'patient');
		})->select(['id', 'name', 'email', 'work_number']);

		
        return Datatables::of($patients)
            ->addColumn('namelink', function ($patients) {
                return '<a href="users/' . $patients->id . '" ">' . $patients->name . '</a>';
            })
            ->addColumn('edit', function ($patients) {
                return '<a href="' . route("users.edit", $patients->id) . '" class="btn btn-success"> Edit</a>';
            })
            ->add_column('delete', function ($patients) { 
                return '<button type="button" class="btn btn-danger delete_client" data-client_id="' . $patients->id . '" onClick="openModal(' . $patients->id. ')" id="myBtn">Delete</button>';
            })->make(true);
    }
}
