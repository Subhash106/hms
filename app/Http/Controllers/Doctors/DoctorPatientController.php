<?php

namespace App\Http\Controllers\Doctors;

use App\Models\Doctor;
use App\Models\Patient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DoctorPatientController extends Controller
{
    public function index(Doctor $doctor)
    {
    	$patients = Patient::all();

    	return view('doctors.patients', compact('patients'));
    }
}
