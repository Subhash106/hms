<?php

namespace App\Http\Controllers\Doctors;

use Datatables;
use App\Models\Doctor;
use App\Models\Appointment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DoctorAppointmentController extends Controller
{
    public function index(Doctor $doctor)
    {
    	$appointments = Appointment::where('doctor_id', $doctor->id)->get();

    	return view('doctors.appointments', compact('appointments'));
    }
}
