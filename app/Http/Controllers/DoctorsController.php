<?php

namespace App\Http\Controllers;

use Datatables;
use App\Models\Doctor;
use Illuminate\Http\Request;

class DoctorsController extends Controller
{
    public function index()
    {
    	$doctors = Doctor::all();
        return view('doctors.index', compact('doctors'));
    }

    public function anyData()
    {
        $doctors = Doctor::whereHas('roles', function($q){
            $q->where('name', 'doctor');
        })->select(['id', 'name', 'email', 'work_number']);
        return Datatables::of($doctors)
            ->addColumn('namelink', function ($doctors) {
                return '<a href="users/' . $doctors->id . '" ">' . $doctors->name . '</a>';
            })
            ->addColumn('edit', function ($doctors) {
                return '<a href="' . route("users.edit", $doctors->id) . '" class="btn btn-success"> Edit</a>';
            })
            ->add_column('delete', function ($doctors) { 
                return '<button type="button" class="btn btn-danger delete_client" data-client_id="' . $doctors->id . '" onClick="openModal(' . $doctors->id. ')" id="myBtn">Delete</button>';
            })->make(true);
    }
}
