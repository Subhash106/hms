<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Doctor extends User
{
    public function appointments()
    {
    	return $this->hasMany('App\Models\Appointment');
    }
}
