    <div class="col-lg-6">
        <!-- Info Boxes Style 2 -->
        <div class="info-box bg-red">
            <span class="info-box-icon"><i class="ion ion-ios-book-outline"></i></span>

            <div class="info-box-content" style="color:#fff;">
                <span class="info-box-text"> All Patients </span>
                <span class="info-box-number">{{$allCompletedTasks}} / {{$alltasks}}</span>

                <div class="progress">
                    <div class="progress-bar" style="width: {{$totalPercentageTasks}}%"></div>
                </div>
                  <span class="progress-description">
                    {{number_format($totalPercentageTasks, 0)}}% {{ __('Completed') }}
                  </span>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>
    <div class="col-lg-6">
        <!-- /.info-box -->
        <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="ion ion-stats-bars"></i></span>

            <div class="info-box-content" style="color:#fff;">
                <span class="info-box-text">All Appointments</span>
                <span class="info-box-number">{{$allCompletedLeads}} / {{$allleads}}</span>

                <div class="progress">
                    <div class="progress-bar" style="width: {{$totalPercentageLeads}}%"></div>
                </div>
                  <span class="progress-description">
                    {{number_format($totalPercentageLeads, 0)}}% {{ __('Completed') }}
                  </span>
            </div>
            <!-- /.info-box-content -->
        </div>

    </div>

    <div class="col-lg-6">
        <!-- Info Boxes Style 2 -->
        <div class="info-box bg-green" style="color:#fff;">
            <span class="info-box-icon"><i class="ion ion-ios-book-outline"></i></span>

            <div class="info-box-content">
                <span class="info-box-text"> All Samples </span>
                <span class="info-box-number">{{$allCompletedTasks}} / {{$alltasks}}</span>

                <div class="progress">
                    <div class="progress-bar" style="width: {{$totalPercentageTasks}}%"></div>
                </div>
                  <span class="progress-description">
                    {{number_format($totalPercentageTasks, 0)}}% {{ __('Completed') }}
                  </span>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>
    <div class="col-lg-6">
        <!-- /.info-box -->
        <div class="info-box bg-aqua" style="color:#fff;">
            <span class="info-box-icon"><i class="ion ion-stats-bars"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">All Reports</span>
                <span class="info-box-number">{{$allCompletedLeads}} / {{$allleads}}</span>

                <div class="progress">
                    <div class="progress-bar" style="width: {{$totalPercentageLeads}}%"></div>
                </div>
                  <span class="progress-description">
                    {{number_format($totalPercentageLeads, 0)}}% {{ __('Completed') }}
                  </span>
            </div>
            <!-- /.info-box-content -->
        </div>

    </div>
</div>

</div>


<!-- Info boxes -->
<div class="row movedown">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-book-outline"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Total Appointments</span>
                <span class="info-box-number">100
                    <small></small></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="ion ion-ios-book-outline"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Total Beds</span>
                <span class="info-box-number">50</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->

    <!-- fix for small devices only -->
    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-stats-bars"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Total Reports</span>
                <span class="info-box-number">{{$completedLeadsToday}}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-stats-bars"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Total Samples</span>
                <span class="info-box-number">{{$createdLeadsToday}}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
    </div>
</div>
<!-- /.info-box -->
    
