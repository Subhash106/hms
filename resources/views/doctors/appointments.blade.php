@extends('layouts.doctor')
@section('heading')
	<h1>All Appointments</h1>
@stop

@section('content')
    <table class="table table-hover" id="appointments-table">
        <thead>
        <tr>
            <th>{{ __('Day') }}</th>
            <th>{{ __('Time') }}</th>
            <th>{{ __('Patient Name') }}</th>
            <th>{{ __('Contact number') }}</th>
        </tr>
        </thead>
        <tbody>
        	@foreach($appointments as $appointment)
        		<td>{{$appointment->appointment_day}}</td>
        		<td>{{$appointment->appointment_time}}</td>
        		<td>{{$appointment->name}}</td>
        		<td>{{$appointment->contact_number}}</td>
        	@endforeach
        </tbody>
    </table>
@stop