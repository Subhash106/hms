@extends('layouts.doctor')
@section('heading')
	<h1>All Patients</h1>
@stop

@section('content')
    <table class="table table-hover" id="patients-table">
        <thead>
        <tr>
            <th>{{ __('Name') }}</th>
            <th>{{ __('Email') }}</th>
            <th>{{ __('Contact number') }}</th>
        </tr>
        </thead>
        <tbody>
        	@foreach($patients as $patient)
                <tr>
        		      <td>{{$patient->name}}</td>
        		      <td>{{$patient->email}}</td>
        		      <td>{{$patient->email}}</td>
                </tr>
        	@endforeach
        </tbody>
    </table>
@stop