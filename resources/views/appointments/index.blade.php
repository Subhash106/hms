@extends('layouts.master')
@section('heading')
    <h1>{{ __('All appointments') }}</h1>
@stop

@section('content')
    <table class="table table-hover" id="appointments-table">
        <thead>
        <tr>
            <th>{{ __('Day') }}</th>
            <th>{{ __('Time') }}</th>
            <th>{{ __('Patient Name') }}</th>
            <th>{{ __('Contact number') }}</th>
        </tr>
        </thead>
    </table>
@stop

@push('scripts')
<script>
    $(function () {
        $('#appointments-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('appointments.data') !!}',
            columns: [
                {data: 'appointment_day', name: 'appointment_day'},
                {data: 'appointment_time', name: 'appointment_time'},
                {data: 'name', name: 'name'},
                {data: 'contact_number', name: 'contact_number'}
            ]
        });
    });
</script>
@endpush